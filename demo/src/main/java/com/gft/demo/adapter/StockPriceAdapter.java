package com.gft.demo.adapter;

import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.model.ReutersStockPriceDetailed;

import java.util.List;

public interface StockPriceAdapter {
    List<ReutersStockListPosition> getStockList();

    ReutersStockPriceDetailed getDetailedStockPrice(String symbol);
}

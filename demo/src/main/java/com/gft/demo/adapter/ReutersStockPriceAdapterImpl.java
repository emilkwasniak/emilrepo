package com.gft.demo.adapter;

import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.model.ReutersStockPriceDetailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

import static com.gft.demo.common.Constant.SYMBOL;

@Service
public class ReutersStockPriceAdapterImpl implements StockPriceAdapter {

    @Value("${stock.price.api.token}")
    private String stockPriceApiToken;

    @Value("${stock.price.url.base}")
    private String stockPriceUrlBase;

    @Value("${stock.price.api.list.suffix.url}")
    private String stockPriceApiListSuffixUrl;

    @Value("${stock.price.api.symbol.suffix.url}")
    private String stockPriceApiSymbolSuffixUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<ReutersStockListPosition> getStockList() {
        String url = getUrlForStockList();
        return Arrays.asList(restTemplate.getForObject(url, ReutersStockListPosition[].class));
    }

    @Override
    public ReutersStockPriceDetailed getDetailedStockPrice(String symbol) {
        String url = getUrlForSymbol(symbol);
        return restTemplate.getForObject(url, ReutersStockPriceDetailed.class);
    }

    protected String getUrlForStockList() {
        return stockPriceUrlBase + stockPriceApiListSuffixUrl + stockPriceApiToken;
    }

    protected String getUrlForSymbol(String symbol) {
        return stockPriceUrlBase + stockPriceApiSymbolSuffixUrl.replace(SYMBOL, symbol) + stockPriceApiToken;
    }

}

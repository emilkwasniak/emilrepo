package com.gft.demo.web.controller;

import com.gft.demo.adapter.StockPriceAdapter;
import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.service.ViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RestController
public class StockController {

	@Autowired
	ViewService viewService;

	@RequestMapping("/")
	public String index() {
		return viewService.getViewString();
	}
	@RequestMapping("/detailed/{symbol}")
	public String getSymbolPage(@PathVariable String symbol) {
		return viewService.getViewStringForSymbol(symbol);
	}

}
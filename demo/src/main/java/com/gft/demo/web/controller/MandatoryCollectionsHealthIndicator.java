package com.gft.demo.web.controller;

import com.gft.demo.database.DatabaseProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class MandatoryCollectionsHealthIndicator implements HealthIndicator {

    @Autowired
    private DatabaseProvider databaseProvider;

    @Override
    public Health health() {
        boolean stockListPositionIsEmpty = CollectionUtils.isEmpty(databaseProvider.getReutersStockListPosition());
        boolean symbolListIsEmpty = CollectionUtils.isEmpty(databaseProvider.getSymbolList());
        if (stockListPositionIsEmpty || symbolListIsEmpty) {
            return Health.down().build();
        } else {
            return Health.up().build();
        }
    }

}
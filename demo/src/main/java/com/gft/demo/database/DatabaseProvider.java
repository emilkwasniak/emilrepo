package com.gft.demo.database;

import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.model.ReutersStockPriceDetailed;

import java.util.List;

public interface DatabaseProvider {
    void put(ReutersStockPriceDetailed reutersStockPriceDetailed);

    ReutersStockPriceDetailed get(String symbol);

    void putInitial(List<ReutersStockListPosition> initialList);

    List<String> getSymbolList();

    List<ReutersStockListPosition> getReutersStockListPosition();
}

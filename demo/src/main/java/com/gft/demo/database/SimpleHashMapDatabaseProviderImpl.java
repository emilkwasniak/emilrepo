package com.gft.demo.database;

import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.model.ReutersStockPriceDetailed;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * To best suit cloud architecture this should be connector to some external database.
 */
@Service
public class SimpleHashMapDatabaseProviderImpl implements DatabaseProvider {

    private Map<String, ReutersStockPriceDetailed> fakeExternalDatabase = new HashMap();
    private List<ReutersStockListPosition> reutersStockListPositions = new ArrayList();

    @Override
    public void put(ReutersStockPriceDetailed reutersStockPriceDetailed) {
        String symbol = reutersStockPriceDetailed.getSymbol();
        ReutersStockPriceDetailed stockPriceDetailedFromDb = fakeExternalDatabase.get(symbol);

        if (stockPriceDetailedFromDb != null && stockPriceDetailedFromDb.getMarketPrice() != null) {
            BigDecimal previousMarketPrice = stockPriceDetailedFromDb.getMarketPrice();
            reutersStockPriceDetailed.setTrend(calculateTrend(previousMarketPrice, reutersStockPriceDetailed));
        }
        fakeExternalDatabase.put(symbol, reutersStockPriceDetailed);
    }

    @Override
    public ReutersStockPriceDetailed get(String symbol) {
        return fakeExternalDatabase.get(symbol);
    }

    @Override
    public void putInitial(List<ReutersStockListPosition> initialList) {
        this.reutersStockListPositions = initialList;
    }

    @Override
    public List<String> getSymbolList() {
        return reutersStockListPositions.stream().map(ReutersStockListPosition::getSymbol).collect(Collectors.toList());
    }

    @Override
    public List<ReutersStockListPosition> getReutersStockListPosition() {
        return reutersStockListPositions;
    }

    protected String calculateTrend(BigDecimal previousMarketPrice, ReutersStockPriceDetailed reutersStockPriceDetailed) {
        int comparisionResult = reutersStockPriceDetailed.getMarketPrice().compareTo(previousMarketPrice);
        if (comparisionResult == 0) {
            return "→";
        } else if (comparisionResult == -1) {
            return "↓";
        } else {
            return "↑";
        }
    }

}

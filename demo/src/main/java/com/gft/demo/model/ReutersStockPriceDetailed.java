package com.gft.demo.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

@Data
public class ReutersStockPriceDetailed {
    private String symbol;
    private BigDecimal iexAskPrice;
    private BigDecimal iexBidPrice;
    private BigDecimal marketPrice;
    private String latestTime;
    private String trend = "";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReutersStockPriceDetailed that = (ReutersStockPriceDetailed) o;
        return symbol.equals(that.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol);
    }
}

package com.gft.demo.model;

import lombok.Data;

@Data
public class ReutersStockListPosition {
    private String symbol;
    private String name;

}

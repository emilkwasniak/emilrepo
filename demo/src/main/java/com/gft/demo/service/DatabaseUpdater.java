package com.gft.demo.service;

public interface DatabaseUpdater {
    int updateDatabase();
}

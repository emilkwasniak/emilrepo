package com.gft.demo.service;

import com.gft.demo.adapter.StockPriceAdapter;
import com.gft.demo.database.DatabaseProvider;
import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.model.ReutersStockPriceDetailed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.lang.String.format;

@Service
@Slf4j
public class DatabaseUpdaterImpl implements DatabaseUpdater {

    private static final int DATABASE_LIMIT = 10;

    @Autowired
    private DatabaseProvider databaseProvider;

    @Autowired
    private StockPriceAdapter stockPriceAdapter;

    @Autowired
    DetailedStockPriceEnhancer detailedStockPriceEnhancer;

    @PostConstruct
    public void postConstruct(){
        log.info("Making initial database fill");
        List<ReutersStockListPosition> limitedStockPrice = stockPriceAdapter.getStockList().subList(0, DATABASE_LIMIT);
        List<ReutersStockListPosition> reutersStockListPositions = limitedStockPrice;
        databaseProvider.putInitial(reutersStockListPositions);
        log.info(format("Database was filled with limited amount of [%s] stocks", limitedStockPrice.size()));
    }

    @Override
    public int updateDatabase(){
        List<String> symbolList = databaseProvider.getSymbolList();
        for (String symbol : symbolList){
            ReutersStockPriceDetailed detailedStockPrice = stockPriceAdapter.getDetailedStockPrice(symbol);
            detailedStockPriceEnhancer.enhanceWithMarketPrice(detailedStockPrice);
            databaseProvider.put(detailedStockPrice);
        }
        return symbolList.size();
    }
}

package com.gft.demo.service;

import com.gft.demo.model.ReutersStockPriceDetailed;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class DetailedStockPriceEnhancerImpl implements DetailedStockPriceEnhancer {

    private static final BigDecimal DIVISOR = BigDecimal.valueOf(2);

    @Override
    public void enhanceWithMarketPrice(ReutersStockPriceDetailed stockPrice) {
        if (stockPrice == null
                || stockPrice.getIexBidPrice() == null
                || stockPrice.getIexAskPrice() == null) {
            throw new RuntimeException("Missing argument");
        } else {
            BigDecimal marketPrice = stockPrice.getIexAskPrice().add(stockPrice.getIexBidPrice()).divide(DIVISOR);
            stockPrice.setMarketPrice(marketPrice);
        }
    }
}

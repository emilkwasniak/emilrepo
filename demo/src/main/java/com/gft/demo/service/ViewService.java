package com.gft.demo.service;

public interface ViewService {
    String getViewString();
    String getViewStringForSymbol(String symbol);
}

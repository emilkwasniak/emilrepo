package com.gft.demo.service;

import com.gft.demo.model.ReutersStockPriceDetailed;

public interface DetailedStockPriceEnhancer {
    void enhanceWithMarketPrice(ReutersStockPriceDetailed reutersStockPriceDetailed);
}

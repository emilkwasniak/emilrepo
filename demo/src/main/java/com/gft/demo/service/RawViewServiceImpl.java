package com.gft.demo.service;

import com.gft.demo.adapter.StockPriceAdapter;
import com.gft.demo.database.DatabaseProvider;
import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.model.ReutersStockPriceDetailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.gft.demo.common.Constant.SYMBOL;

@Service
public class RawViewServiceImpl implements ViewService {

    private final static String TITLE = "{TITLE}";
    private final static String URL = "{URL}";
    private final static String A_HREF_TEMPLATE = " <a href=\"" + URL + "\">" + TITLE + "</a><br/>";
    public static final String BR = "<br/>";

    @Value("${stock.price.local.url}")
    private String stockPriceLocalUrl;

    @Autowired
    DatabaseProvider databaseProvider;

    @Override
    public String getViewString() {
        List<ReutersStockListPosition> stockList = databaseProvider.getReutersStockListPosition();
        String result = "";
        for (ReutersStockListPosition position : stockList) {
            result += A_HREF_TEMPLATE.replace(TITLE, makeUrlTitle(position)).replace(URL, makeUrl(position.getSymbol()));
        }
        return result;
    }

    @Override
    public String getViewStringForSymbol(String symbol) {
        ReutersStockPriceDetailed detailedStockPrice = databaseProvider.get(symbol);

        return "Symbol: " + detailedStockPrice.getSymbol() + BR +
                "Ask Price: " + detailedStockPrice.getIexAskPrice() + BR +
                "Bid Price: " + detailedStockPrice.getIexBidPrice() + BR +
                "Market Price: " + detailedStockPrice.getMarketPrice() + BR +
                "Event time: " + detailedStockPrice.getLatestTime() + BR +
                "Trend: " + detailedStockPrice.getTrend() + BR;
    }

    private String makeUrl(String symbol) {
        return stockPriceLocalUrl.replace(SYMBOL, symbol);
    }

    private String makeUrlTitle(ReutersStockListPosition position) {
        return position.getName() + ", [" + position.getSymbol() + "] ";
    }
}

package com.gft.demo.configuration.scheduler;

import com.gft.demo.service.DatabaseUpdater;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
@Slf4j
public class ScheduledTasks {

	@Autowired
	private DatabaseUpdater databaseUpdater;

	@Scheduled(fixedRate = 60*1000)
	public void scheduledUpdate() {
		int size = databaseUpdater.updateDatabase();
		log.info(format("Updated [%s] stocks!", size));
	}
}
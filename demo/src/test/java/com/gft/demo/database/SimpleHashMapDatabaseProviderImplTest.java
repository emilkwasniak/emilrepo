package com.gft.demo.database;

import com.gft.demo.model.ReutersStockListPosition;
import com.gft.demo.model.ReutersStockPriceDetailed;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SimpleHashMapDatabaseProviderImplTest {

    SimpleHashMapDatabaseProviderImpl provider = new SimpleHashMapDatabaseProviderImpl();

    @Test
    public void testInitialFlow() {
        List<ReutersStockListPosition> reutersStockListPositions = Arrays.asList(makeReutersStockListPosition("MS"),
                makeReutersStockListPosition("ABC"));
        provider.putInitial(reutersStockListPositions);
        List<String> symbolList = provider.getSymbolList();
        assertEquals(2, symbolList.size());
        assertTrue(symbolList.contains("MS"));
        assertTrue(symbolList.contains("ABC"));
    }

    @Test
    public void calculateTrend() {
        BigDecimal previousMarketPrice = BigDecimal.valueOf(3.0);
        assertEquals("→", provider.calculateTrend(previousMarketPrice, makeReutersStockPriceDetailed(3.0)));
        assertEquals("↑", provider.calculateTrend(previousMarketPrice, makeReutersStockPriceDetailed(4.0)));
        assertEquals("↓", provider.calculateTrend(previousMarketPrice, makeReutersStockPriceDetailed(1.0)));
    }

    @Test
    public void testUpdateFlow(){
        ReutersStockPriceDetailed reutersStockPriceDetailed = makeReutersStockPriceDetailed("IBM", 2.0);
        provider.put(reutersStockPriceDetailed);

        reutersStockPriceDetailed = provider.get("IBM");
        assertEquals("", reutersStockPriceDetailed.getTrend());

        ReutersStockPriceDetailed changed = makeReutersStockPriceDetailed("IBM", 2.0);
        provider.put(changed);

        assertNotNull(provider.get("IBM").getTrend());
    }

    private ReutersStockPriceDetailed makeReutersStockPriceDetailed(double actualPrice){
        ReutersStockPriceDetailed reutersStockPriceDetailed = new ReutersStockPriceDetailed();
        reutersStockPriceDetailed.setMarketPrice(new BigDecimal(actualPrice));
        return reutersStockPriceDetailed;
    }

    private ReutersStockListPosition makeReutersStockListPosition(String symbol){
        ReutersStockListPosition position = new ReutersStockListPosition();
        position.setSymbol(symbol);
        return position;
    }

    private ReutersStockPriceDetailed makeReutersStockPriceDetailed(String symbol, double marketPrice){
        ReutersStockPriceDetailed reutersStockPriceDetailed = new ReutersStockPriceDetailed();
        reutersStockPriceDetailed.setSymbol(symbol);
        reutersStockPriceDetailed.setMarketPrice(BigDecimal.valueOf(marketPrice));
        return reutersStockPriceDetailed;
    }
}
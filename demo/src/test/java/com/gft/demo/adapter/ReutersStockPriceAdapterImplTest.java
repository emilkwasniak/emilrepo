package com.gft.demo.adapter;

import com.gft.demo.model.ReutersStockListPosition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReutersStockPriceAdapterImplTest {

    @InjectMocks
    ReutersStockPriceAdapterImpl reutersStockPriceAdapter;

    @Mock
    RestTemplate restTemplate;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(reutersStockPriceAdapter, "stockPriceApiToken", "someToken");
        ReflectionTestUtils.setField(reutersStockPriceAdapter, "stockPriceUrlBase", "http://base");
        ReflectionTestUtils.setField(reutersStockPriceAdapter, "stockPriceApiListSuffixUrl", "listSuffixUrl");
        ReflectionTestUtils.setField(reutersStockPriceAdapter, "stockPriceApiSymbolSuffixUrl", "symbolSuffixUrl");
    }

    @Test
    public void getStockList() {
        when(restTemplate.getForObject("http://baselistSuffixUrlsomeToken", ReutersStockListPosition[].class))
                .thenReturn(new ReutersStockListPosition[]{createPosition()});

        List<ReutersStockListPosition> stockList = reutersStockPriceAdapter.getStockList();
        assertEquals(1, stockList.size());
        assertEquals(createPosition(), stockList.get(0));
    }

    @Test
    public void getDetailedStockPrice() {
    }

    @Test
    public void getUrlForStockList() {
        assertEquals("http://baselistSuffixUrlsomeToken", reutersStockPriceAdapter.getUrlForStockList());
    }

    @Test
    public void getUrlForSymbol() {
        assertEquals("http://basesymbolSuffixUrlsomeToken", reutersStockPriceAdapter.getUrlForSymbol("fancySymbol"));
    }

    private ReutersStockListPosition createPosition() {
        ReutersStockListPosition position = new ReutersStockListPosition();
        position.setName("IBM name");
        position.setSymbol("IBM");
        return position;
    }
}